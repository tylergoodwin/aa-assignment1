import java.io.PrintStream;
import java.util.*;

public class BstMultiset<T> extends Multiset<T>
{
	protected BstNode<T> bRoot;
	protected int bNum;
	
	public BstMultiset() {
		bRoot = null;
		bNum = 0;
	} // end of BstMultiset()

	public void add(T item) {
		boolean done = false;
		BstNode<T> parent = bRoot;
		BstNode<T> currNode = bRoot;
		BstNode<T> newNode = new BstNode<T>(item);
		
		String currVal, addMe;
				
		
		//Case tree is empty
		if (bRoot == null)
		{
			bRoot = newNode;
			done = true;
		}

		addMe = String.valueOf(item);
	
		while (!done)
		{
			currVal = String.valueOf(currNode.getElement());
			parent = currNode;
			// left
			if (currVal.compareTo(addMe) > 0) 
			{	
				currNode = currNode.left;
				if (currNode == null)
				{
					parent.setLeft(newNode);
					done = true;
				}
			}
			// right
			else if(currVal.compareTo(addMe) < 0)
			{
				currNode = currNode.right;
				if (currNode == null)
				{
					parent.setRight(newNode);
					done = true;
				}
			}
			else if (currVal.compareTo(addMe) == 0)
			{
				currNode.setAmount(currNode.getAmount() + 1);
				done = true;
			}	
		}
		
	} // end of add()


	public int search(T item) 
	{
		BstNode<T> found;
		found = GetNode(item);
		if (found != null)
		{
			return found.getAmount();
		}
		else
			return 0;
	
	} 
// end of search()

	public void removeOne(T item) 
	{
		
		BstNode<T> currNode = bRoot;
		BstNode<T> parent = bRoot;
		String currVal;
		
		boolean done = false;
		String removeMe = String.valueOf(item);
		currVal = String.valueOf(currNode.getElement());
		int check;
		
		//Remove root
		if (currVal.compareTo(removeMe) == 0)
		{
			if (currNode.getAmount() == 1)
			{
				check = RemoveRoot();
				if (check == 1)
				{
					return;
				}
			}
			else
			{
				currNode.setAmount(currNode.getAmount() - 1);
				return;
			}
		}
		
		if (search(item) == 0)
		{
			return;
		}
		
		while (!done)
		{
			currVal = String.valueOf(currNode.getElement());
			parent = currNode;
			// left
			if (currVal.compareTo(removeMe) > 0) 
			{	
				currNode = currNode.left;
				currVal = String.valueOf(currNode.getElement());
				if (currVal.compareTo(removeMe) == 0)
				{
					if (currNode.getAmount() == 1)
					{
						if (currNode.getLeft() != null)
						{
							parent.setLeft(currNode.getLeft());
						}
						else
						{
							parent.setLeft(null);
						}
						done = true;
					}
					else
					{
						currNode.setAmount(currNode.getAmount() - 1);
						done = true;
					}
				}
			}
			// right
			else if(currVal.compareTo(removeMe) < 0)
			{
				currNode = currNode.right;
				currVal = String.valueOf(currNode.getElement());
				if (currVal.compareTo(removeMe) == 0)
				{
					if (currNode.getAmount() == 1)
					{
						if (currNode.getRight() != null)
						{
							parent.setRight(currNode.getRight());
						}
						else
						{
							parent.setRight(null);
						}
						done = true;
					}
					else
					{
						currNode.setAmount(currNode.getAmount() - 1);
						done = true;
					}
				}
			}	
		}
		
		
	} // end of removeOne()
	
	
	public void removeAll(T item) 
	{
		
		BstNode<T> currNode = bRoot;
		BstNode<T> parent = bRoot;
		
		boolean done = false;
		String removeMe = String.valueOf(item);
		String currVal = String.valueOf(currNode.getElement());
		int check = 0;
		
		//Remove Root Node
		if (currVal.compareTo(removeMe) == 0)
		{
			check = RemoveRoot();
			if (check == 1)
			{
				return;
			}
		}
		if (search(item) == 0)
		{
			return;
		}
		
		while (!done)
		{
			parent = currNode;
			// left
			if (currVal.compareTo(removeMe) > 0) 
			{	
				currNode = currNode.left;
				currVal = String.valueOf(currNode.getElement());		
				if (currVal.compareTo(removeMe) == 0)
				{
					if (currNode.getLeft() != null)
					{
						parent.setLeft(currNode.getLeft());						
					}
					else
					{
						parent.setLeft(null);
					}
					done = true;
				}
			}
			// right
			else if(currVal.compareTo(removeMe) < 0)
			{
				currNode = currNode.right;
				currVal = String.valueOf(currNode.getElement());		
				if (currVal.compareTo(removeMe) == 0)
				{
					if (currNode.getRight() != null)
					{
						parent.setRight(currNode.getRight());
					}
					else
					{
						parent.setRight(null);
					}
					done = true;
				}

			currVal = String.valueOf(currNode.getElement());		
			}
		}
	} // end of removeAll()


	public void print(PrintStream out) 
	{
			
		//Make print node function.	
		printNode(bRoot, out);

	} // end of print()

	public BstNode GetNode(T item)
	{
		BstNode<T> currNode = bRoot;
		String findMe = String.valueOf(item);
		String currVal = String.valueOf(currNode.getElement());
		
		//Want root?
		if (currVal.compareTo(findMe) == 0)
		{
			return currNode;
		}
		
		//Search left
		while (currNode.getLeft() != null && currVal.compareTo(findMe) > 0)
		{
			currNode = currNode.getLeft();
			currVal = String.valueOf(currNode.getElement());
			while (currNode.getRight() != null && currVal.compareTo(findMe) < 0)
			{
				currNode = currNode.getRight();
				currVal = String.valueOf(currNode.getElement());
			}
			if (currVal.compareTo(findMe) == 0)
			{
				return currNode;
			}
		}
		//Search right
		while (currNode.getRight() != null && currVal.compareTo(findMe) < 0)
		{
			currNode = currNode.getRight();
			currVal = String.valueOf(currNode.getElement());
			while (currNode.getLeft() != null && currVal.compareTo(findMe) > 0)
			{
				currNode = currNode.getLeft();
				currVal = String.valueOf(currNode.getElement());
			}
			if (currVal.compareTo(findMe) == 0)
			{
				return currNode;
			}
		}
		return null;
	}
	
	public int RemoveRoot()
	{
		BstNode<T> currNode = bRoot;
		BstNode<T> leftBranch;
		
		bRoot.setAmount(0);
		
		if (currNode.getLeft() != null)
		{
			leftBranch = currNode.getLeft();
			if (currNode.getRight() != null)
			{
				currNode = currNode.getRight();
				while (currNode.getLeft() != null)
				{
					currNode = currNode.getLeft();
				}
				currNode.setLeft(leftBranch);
				bRoot = bRoot.getRight();
				return 1;
			}
			else
			{
				bRoot = bRoot.getLeft();
				return 1;
			}
			
		}
		
		return 0;
		
	}
	
	public void printNode(BstNode<T> in, PrintStream out)
	{
		
		
		out.println("" + in.getElement() + " |" + in.getAmount());
		
		if (in.getLeft() != null )
		{
			printNode(in.getLeft(), out);
		}
		
		if (in.getRight() != null)
		{
			printNode(in.getRight(), out);
		}
		
	}
} // end of class BstMultiset
