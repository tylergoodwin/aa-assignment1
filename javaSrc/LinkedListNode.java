/*
*	LinkedListNode.java
*
*	node for storing data in linked list and sorted linked lists
*
*
*/

public class LinkedListNode<T> {
	
	protected T element;
	protected int amount;
	protected LinkedListNode next;
	
	public LinkedListNode(T item){
		element = item;
		amount = 1;
		next = null;		
	}
	
	public T getElement(){
		return element;
	}
	
	public int getAmount(){
		return amount;
	}
	
	public LinkedListNode getNext(){
		return next;
	}
	
	public void setNext(LinkedListNode new_next){
		next = new_next;
	}
	
	public void setAmount(int new_amount){
		amount = new_amount;
	}
	
	
}