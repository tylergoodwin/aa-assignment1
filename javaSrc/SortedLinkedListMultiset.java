import java.io.PrintStream;
import java.util.*;

public class SortedLinkedListMultiset<T> extends Multiset<T>
{
	protected LinkedListNode<T> mHead;
	protected int mLength;
	
	public SortedLinkedListMultiset() {
		mHead = null;
	} // end of SortedLinkedListMultiset()
	
	
	public void add(T item) {
		boolean added = false;
		
		LinkedListNode<T> currentNode = mHead;
		LinkedListNode<T> newNode = new LinkedListNode<T>(item);
		
		if(mHead == null){
			mHead = newNode;
			added = true;
		}
		else {
			//step through list to find the correct position for the added item
			
			//if item < head item
			int comp = compareItems(item, mHead.getElement());
			if(comp < 0){
				
				newNode.setNext(mHead);
				mHead = newNode;
				added = true;
			}
			//if item = head item
			else if(comp == 0)
				mHead.setAmount(mHead.getAmount() + 1);
			// if item belongs after head item, ie comp > 0
			else {
				if(mHead.getNext() == null){
					mHead.setNext(new LinkedListNode(item));
					added = true;
				}
				//step through list until position is found
				while(currentNode != null && added == false){
					if(currentNode.getNext() == null){
							currentNode.setNext(newNode);
							added = true;
					}
					else {
						comp = compareItems(item, (currentNode.getNext()).getElement());
						if(comp < 0){
							//insert between current and next
							newNode.setNext(currentNode.getNext());
							currentNode.setNext(newNode);
							added = true;
						}
						else if(comp == 0){
							//increment next
							currentNode.getNext().setAmount(currentNode.getNext().getAmount() + 1);
							added = true;
						}
						else {
							//go to next node
							currentNode = currentNode.getNext();
							
						}
					}
				}
			}
			
		}
	} // end of add()
	
	
	public int search(T item) {
		
		LinkedListNode<T> currentNode = mHead;
		
		while(currentNode != null){
			if((currentNode.getElement()).equals(item)){
				return currentNode.getAmount();
			}
			else {
				currentNode = currentNode.getNext();
			}
		}		
		return 0;
		
	} // end of add()
	
	
	public void removeOne(T item) {
		LinkedListNode<T> currentNode = mHead;
		LinkedListNode<T> previousNode = null;
		boolean found = false;
		
		while(currentNode != null && !found){
			if(currentNode.getElement().equals(item)){
				found = true;
				
				if(currentNode.getAmount() > 1){
					currentNode.setAmount(currentNode.getAmount()-1);
				}
				else { //only 1 left of an item, therefore node needs to be removed
					if(currentNode != mHead){
						previousNode.setNext(currentNode.getNext());
					}
					else { //currentNode == mHead
						mHead = currentNode.getNext();
					}
				}
			}
			else {
				previousNode = currentNode;
				currentNode = currentNode.getNext();
			}
		}
	} // end of removeOne()
	
	
	public void removeAll(T item) {
		LinkedListNode<T> previousNode = null;
		LinkedListNode<T> currentNode = mHead;
		boolean found = false;
		
		while(currentNode != null && !found){
			if(currentNode.getElement().equals(item)){
				found = true;
				if(currentNode != mHead){
					previousNode.setNext(currentNode.getNext());
				}
				else { //currentNode == mHead
					mHead = currentNode.getNext();
				}
			}
			else{
				previousNode = currentNode;
				currentNode = currentNode.getNext();
			}
		}
	} // end of removeAll()
	
	
	public void print(PrintStream out) {
		LinkedListNode<T> currentNode = mHead;
		while(currentNode != null){
			
			out.println(currentNode.getElement() + printDelim + currentNode.getAmount());
			currentNode = currentNode.getNext();
		}
	} // end of print()
	
	public int compareItems(T item_1, Object item_2){
		return String.valueOf(item_1).compareTo(String.valueOf(item_2));
	}
	
} // end of class SortedLinkedListMultiset